package com.group31.androidchess;

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Keeps track of all saved games. Handles data storage.
 *
 * @author Feiying Zheng (fz95)
 * @author Kevin Cadavillo(kac460)
 */
public class AllGames implements Serializable {
    /**
     * Stores the file name for the serialized data.
     */
    public static final String storeFile = "games.dat";
    public static ArrayList<Game> allGames=new ArrayList<Game>();
    public static Context context;
    /**
     * Serializes the list of games.
     */
    public static void saveGames() throws IOException {
        //System.out.println("SAVING GAME");

        FileOutputStream fos = context.openFileOutput(storeFile, Context.MODE_PRIVATE);
        ObjectOutputStream os = new ObjectOutputStream(fos);
        os.writeObject(allGames);
        os.close();
        fos.close();

    }

    public static void loadGames() throws IOException, ClassNotFoundException {

        //System.out.println("PRINTING Games");

        File file = new File(context.getFilesDir(),storeFile);
        ///create the file if it doesnt exist
        if(!file.exists()){
            file.createNewFile();
            //System.out.println("saved games file doesnt exist. Creating new file: "+file.getAbsolutePath());
        }
        //load if there are saved games (file is not empty
        if(file.length()!=0){
            FileInputStream fis=new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            allGames = (ArrayList<Game>)ois.readObject();
        }else{
            //System.out.println("NO GAMES");
        }

        printGames();

    }


    public static void printGames(){
        for (int i=0; i<allGames.size();i++){
            //System.out.println(allGames.get(i).title+" "+allGames.get(i).date);
        }

    }

}
