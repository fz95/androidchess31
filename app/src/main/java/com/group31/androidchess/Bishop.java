package com.group31.androidchess;
/**
 * Represents the Bishop Piece.
 *
 * @author Feiying Zheng (fz95)
 * @author Kevin Cadavillo(kac460)
 */
public class Bishop extends Piece {

    /**
     * 3 argument constructor, usually used for making a new Bishop piece when a new Chess game starts.
     *
     * @param currentRank initial rank of the Bishop
     * @param currentFile initial file of the Bishop
     * @param color color of the Bishop (b or w)
     */
    public Bishop(int currentRank, int currentFile, char color) {
        super(currentRank, currentFile, color);
    }

    /**
     * 1 argument constructor, makes a new Bishop piece that is identical to the given piece (same rank and file).
     * Usually used when creating a hypothetical board that is identical to the current Chess board.
     *
     * @param b the Bishop that you want to copy.
     */
    public Bishop(Piece b){
        super(b);
    }

    public boolean canMove(int endRank, int endFile, Piece[][] board) {
        return Rules.diagRuleCheck(currentRank, currentFile, endRank, endFile, board);
    }

    public int display(){
        if (this.color=='b'){
            return R.drawable.black_bishop;
        }
        return R.drawable.white_bishop;
    }
    public String toString() {
        return color+"B";
    }
}
