package com.group31.androidchess;
import java.util.Scanner;

/**
 * Represents the Chess game.
 *
 * @author Feiying Zheng (fz95)
 * @author Kevin Cadavillo(kac460)
 */
public class Chess {

    /**
     * Stores the pawn of the previous move that made a double advance.
     * If the previous move was not on a pawn, or it was a pawn but it did not double advance, it will be null.
     * Having this set indicates that the next player can play en passant. If the next player does not, then it will be set to null again.
     *
     */
    public static Piece doubleAdvancePawn=null;

    /**
     * Chess game occurs here. Takes inputs from the two players and displays the board after each legal move.
     */
    /**
     * Tracks the state of the board during the game.
     */
    public static Piece[][] board=null;

    public static void start(){
        board = new Piece[8][8];
        populate(board);
        printBoard(board);
    }
    /**
     * If 0, game is not over.
     * if 1, draw.
     * if 2, black won.
     * if 3, white won.
     * if 4, current player in check.
     */
    public static int checkGameStatus(char turn){
        int gameStatus=0;

        if(hasNoLegalMoves(board, turn)){

            //Checkmate occurs when player is in check and there are no legal moves
            if(inCheck(board, turn)){
                //System.out.println("Checkmate");

                if(turn=='w') {
                    //System.out.println("Black wins");
                    gameStatus=2;
                }
                else {
                    //System.out.println("White wins");
                    gameStatus=3;
                }
            }
            else{
                //Stalemate occurs when player is NOT in check and there are no legal moves
                //System.out.println("Stalemate");
                //System.out.println("draw");
                gameStatus=1;
            }
            return gameStatus;
        }
        //if get here, there is still legal moves. See if current turn is in check
        if(inCheck(board, turn)){
            return 4;
        }else {
            return gameStatus;
        }
    }

    public static boolean executeMove(int startRank, int startFile, int endRank, int endFile, char turn ){

        if(inCheck(board, turn))
            //System.out.println("Check");
        if(turn=='w') {
            //System.out.print("White's move: ");
        }else {
            //System.out.print("Black's move: ");
        }

        /*if(s.equals("resign")){
            if(turn=='w')
                //System.out.println("Black wins");
            else
                //System.out.println("White wins");
            return;
        }
*/
        //ONLY input if previous player asked "draw?" -> will draw.
       /* if(s.equals("draw")){
            //System.out.println("draw");
            return;
        }*/
        /*int[] indices=convertInputToIndices(s);

        int startRank=indices[0];
        int startFile=indices[1];
        int endRank=indices[2];
        int endFile=indices[3];

        //Need to check inputs here so will not get array index out of bound when accessing board
        if(startRank > 7 || startFile > 7 || endRank > 7 || endFile >7) {
            //System.out.println("Illegal move, try again");
            continue;
        }
        if(startRank < 0 || startFile < 0 || endRank < 0 || endFile < 0) {
            //System.out.println("Illegal move, try again");
            continue;
        }
        */
        Piece piece=board[startRank][startFile];

        if(isLegalMove(startRank, startFile, endRank, endFile, board, turn)) {
            piece.currentRank=endRank;
            piece.currentFile=endFile;
            board[endRank][endFile]=piece;
            board[startRank][startFile]=null;

            //execute rook's castling move if appropriate:
            if(piece instanceof King) {
                if(((King) piece).castling) {
                    ((King)piece).castling=false;
                    if(endFile<startFile) { //king move 2 left, rook move 3 right
                        board[startRank][0].currentFile=3;
                        board[startRank][3]=board[startRank][0];
                        board[startRank][0]=null;
                        ((Rook)board[startRank][3]).hasMoved=true;
                    }else { //king move 2 right, rook move 2 left
                        board[startRank][7].currentFile=5;
                        board[startRank][5]=board[startRank][7];
                        board[startRank][7]=null;
                        ((Rook)board[startRank][5]).hasMoved=true;
                    }
                }
                ((King)piece).hasMoved=true;
            }

            //Need to set Rook's hasMoved field even when castling does not occur.
            if(piece instanceof Rook)
                ((Rook)piece).hasMoved=true;


            //current pawn double advanced, so set doubleAdvancePawn
            if(piece instanceof Pawn && Math.abs(startRank-endRank)==2) {
                doubleAdvancePawn=piece;
            }else {
                doubleAdvancePawn=null;
            }

            //execute en passant move if appropriate:
            if(piece instanceof Pawn && ((Pawn)piece).enPassant) {
                board[startRank][endFile]=null;
                ((Pawn)piece).enPassant=false;
            }

            //promote(endRank, endFile, board, s);

            if(turn=='w')
                turn='b';
            else
                turn='w';

            //System.out.println();
            printBoard(board);
            return true;
        }
        else {
            //System.out.println("Illegal move, try again");
            return false;
        }

    }

    /**
     * Converts input string to rank and file indexes of the chess board.
     * @param s input string of the form "FileRank FileRank"
     * @return new int[] {startRank, startFile, endRank, endFile}
     */
    private static int[] convertInputToIndices(String s) {
        int startFile=convertFileToIndex(s.charAt(0));
        int startRank=convertRankToIndex(s.charAt(1));
        int endFile=convertFileToIndex(s.charAt(3));
        int endRank=convertRankToIndex(s.charAt(4));
        return new int[] {startRank, startFile, endRank, endFile};

    }

    private static int convertFileToIndex(char file) {

        return (int)file - 97;
    }

    private static int convertRankToIndex(char rank) {

        return -1*((int)rank -56);
    }

    /**
     * Populates the board with the chess pieces in the correct locations.
     * @param board the board that you want to populate.
     */
    private static void populate(Piece[][] board) {
        //pawns
        for(int i=0; i<8; i++) {
            board[6][i]=new Pawn(6, i, 'w');
            board[1][i]=new Pawn(1, i, 'b');

        }
        //rooks
        board[0][0]=new Rook(0,0, 'b');
        board[0][7]=new Rook(0,7, 'b');
        board[7][0]=new Rook(7,0, 'w');
        board[7][7]=new Rook(7,7, 'w');

        //knights
        board[0][1]=new Knight(0,1, 'b');
        board[0][6]=new Knight(0,6, 'b');
        board[7][1]=new Knight(7,1, 'w');
        board[7][6]=new Knight(7,6, 'w');

        //bishops
        board[0][2]=new Bishop(0,2, 'b');
        board[0][5]=new Bishop(0,5, 'b');
        board[7][2]=new Bishop(7,2, 'w');
        board[7][5]=new Bishop(7,5, 'w');

        //Queen
        board[0][3]=new Queen(0,3, 'b');
        board[7][3]=new Queen(7,3, 'w');

        //King
        board[0][4]=new King(0,4, 'b');
        board[7][4]=new King(7,4, 'w');
    }

    /**
     * Prints out the board and the chess pieces.
     * @param board the board that you want printed.
     */
    public static void printBoard(Piece[][] board) {

        boolean isBlack=true;
        for(int i=0; i<8; i++) {
            isBlack=!isBlack;
            for(int j=0; j<8; j++) {
                if(board[i][j]==null) {
                   // if(isBlack)
                        //System.out.print("##");
                    //else
                        //System.out.print("  ");
                }
                else {
                    //System.out.print(board[i][j]);
                }
                //System.out.print(" ");
                isBlack=!isBlack;
            }
            //System.out.println(-1*(i-8));
        }
        //System.out.println(" a  b  c  d  e  f  g  h");
    }

    /**
     * Checks to see if a player's move is legal, and makes sure move does not put player's king in check.
     *
     * @param startRank starting rank of the piece that the player wants to move.
     * @param startFile starting file of the piece that the player wants to move.
     * @param endRank rank where the player wants to move the piece to.
     * @param endFile file where the player wants to move the piece to.
     * @param board the board of the current Chess game
     * @param currColor the color of the player ('w' or 'b') that is making the move.
     *
     * @return true if the move is legal, false if the move is illegal.
     */
    public static boolean isLegalMove(int startRank, int startFile, int endRank, int endFile, Piece[][] board, char currColor) {

        //out of range
        if(startRank > 7 || startFile > 7 || endRank > 7 || endFile >7)
            return false;
        if(startRank < 0 || startFile < 0 || endRank < 0 || endFile < 0)
            return false;

        //cannot move to current spot
        if(startRank==endRank && startFile==endFile)
            return false;


        Piece piece=board[startRank][startFile];

        //cannot move empty square
        if(piece==null)
            return false;

        //cannot move opponent's color
        if(piece.color!=currColor)
            return false;

        if(!piece.canMove(endRank,endFile, board)) {
            return false;
        }

        //cannot move to a spot where another one of your pieces is located
        if(board[endRank][endFile]!=null && board[endRank][endFile].color==currColor)
            return false;

        //Simulate move in another board and check if it puts the current player in check
        Piece[][] hypBoard = deepCopyBoard(board);
        Piece hypPiece=hypBoard[startRank][startFile];

        //Check to see if castling causes any problems
        if(hypPiece instanceof King) {
            if(((King)hypPiece).castling) {
                ((King)hypPiece).castling=false;
                if(endFile<startFile) { //king move 2 left, rook move 3 right
                    //Rook's movement:
                    if(!hypBoard[startRank][0].canMove(startRank, 3, hypBoard))
                        return false;
                    hypBoard[startRank][0].currentFile=3;
                    hypBoard[startRank][3]=hypBoard[startRank][0];
                    hypBoard[startRank][0]=null;
                }else { //king move 2 right, rook move 2 left
                    if(!hypBoard[startRank][7].canMove(startRank, 5, hypBoard))
                        return false;
                    //Rook's movement:
                    hypBoard[startRank][7].currentFile=5;
                    hypBoard[startRank][5]=hypBoard[startRank][7];
                    hypBoard[startRank][7]=null;
                }
            }
        }

        //See if en passant move causes any problems:
        if(hypPiece instanceof Pawn && ((Pawn)hypPiece).enPassant) {
            hypBoard[startRank][endFile]=null;
            ((Pawn)hypPiece).enPassant=false;
        }


        //update the moved piece and test if move caused a check for current player
        hypBoard[startRank][startFile].currentRank=endRank;
        hypBoard[startRank][startFile].currentFile=endFile;
        hypBoard[endRank][endFile]=board[startRank][startFile];
        hypBoard[startRank][startFile]=null;

        if(inCheck(hypBoard, currColor)==true)
            return false;

        return true;
    }

    /**
     * Promotes the Pawn at the given rank and file as appropriate
     *
     * @param rank the rank of the promoted piece.
     * @param file the file of the promoted piece.
     * @param newRole the player's input string of the form "FileRank FileRank X", where X is the piece to promote to, or just "FileRank FileRank", which will automatically promote to a Queen.
     *
     */
    public static void promote(int rank, int file,/* Piece[][] board, */char newRole){
        Piece piece=board[rank][file];
        /*
        if(!(piece instanceof Pawn))
            return;
        if(!(rank==0 || rank==7))
            return;*/
        //note that promotion is NOT optional! Cannot stay a pawn!
        /*if(input.length()>6){
            char newRole=input.charAt(6);
            if(newRole=='R'){
                board[rank][file] = new Rook(piece);
                ((Rook)board[rank][file]).hasMoved=true; //CANNOT use a promoted rook to castle
            }
            else if(newRole=='N')
                board[rank][file] = new Knight(piece);
            else if(newRole=='B')
                board[rank][file] = new Bishop(piece);
            else
                board[rank][file] = new Queen(piece);
        }
        else
            board[rank][file] = new Queen(piece); */
        if(newRole=='R'){
            board[rank][file] = new Rook(piece);
            ((Rook)board[rank][file]).hasMoved=true; //CANNOT use a promoted rook to castle
        }
        else if(newRole=='N')
            board[rank][file] = new Knight(piece);
        else if(newRole=='B')
            board[rank][file] = new Bishop(piece);
        else
            board[rank][file] = new Queen(piece);

    }

    /**
     * Determines if a piece is a pawn and needs to be promoted
     *
     * @param rank the rank of the piece.
     * @param file the file of the piece.
     * @return true if it needs to be promoted, false otherwise
     *
     */
    public static boolean canPromote(int rank, int file){
        Piece piece=board[rank][file];
        if(!(piece instanceof Pawn))
            return false;
        if(!(rank==0 || rank==7))
            return false;
        return true;
    }

    /**
     * Determines if a player is in check from a board.
     * USAGE NOTES:
     *  1) Can be used to make sure a hypothetical move by a player p would not cause p to be in check.
     *  2) Can be used to make sure a hypothetical move by a currently checked player p would break p out of check.
     *  3) For the board parameter, in main, we could create a DEEPCOPY of the current board and then move on that board
     *  	(do not want to change actual game state)
     *  	(must be a deepCopy so that we do not mess with the real game pieces!)
     * @param hypBoard any HYPOTHETICAL chess game state, not strictly the one in play.
     * @param currPlayer the color of the player ('w' or 'b') that you want to determine if they are in check.
     *
     * @return true if the player is in check, false if not.
     */
    public static boolean inCheck(Piece[][] hypBoard, char currPlayer){

        char opponent='w';
        if(currPlayer=='w')
            opponent='b';

        int[] kingPos=findKing(hypBoard, currPlayer);
        int kingRank=kingPos[0];
        int kingFile=kingPos[1];

        for(int i=0; i<8; i++){
            for(int j=0; j<8; j++){
                if(hypBoard[i][j]!=null && hypBoard[i][j].color==opponent
                        && hypBoard[i][j].canMove(kingRank, kingFile, hypBoard))
                    return true;
            }
        }
        return false;
    }
    /**
     * Determines the rank and file of the player's king
     *
     * @param hypBoard the board of the Chess game
     * @param currPlayer the color of the player ('w' or 'b').
     * @return new int[2] of the king's rank and file.
     */
    public static int[] findKing(Piece[][] hypBoard, char currPlayer){

        int kingRank=0;
        int kingFile=0;

        for(int i=0; i<8; i++){
            for(int j=0; j<8; j++){
                if(hypBoard[i][j] instanceof King && hypBoard[i][j].color==currPlayer){
                    kingRank=i;
                    kingFile=j;
                    return new int[] {kingRank, kingFile};
                }
            }
        }
        //should never reach here
        return null;
    }

    /**
     *Creates an identical DEEPCOPY of the current board.
     *The identical board (hypothetical board) can allow program to make moves to see if it is legal or causes a check without changing the current state of the actual board.
     *
     * @param board the board of the current Chess game
     * @return A new Piece[8][8] identical to the current board.
     */
    public static Piece[][] deepCopyBoard(Piece[][] board){

        Piece newBoard[][] = new Piece[8][8];
        for(int i=0; i<8; i++){
            for(int j=0; j<8; j++){
                if(board[i][j] instanceof Bishop)
                    newBoard[i][j]=new Bishop(board[i][j]);

                else if(board[i][j] instanceof King)
                    newBoard[i][j]=new King(board[i][j]);

                else if(board[i][j] instanceof Knight)
                    newBoard[i][j]=new Knight(board[i][j]);

                else if(board[i][j] instanceof Pawn)
                    newBoard[i][j]=new Pawn(board[i][j]);

                else if(board[i][j] instanceof Queen)
                    newBoard[i][j]=new Queen(board[i][j]);

                else if(board[i][j] instanceof Rook)
                    newBoard[i][j]=new Rook(board[i][j]);
            }
        }
        return newBoard;
    }

    /**
     * Checks to see if a given player has no legal moves to make.
     * Used for Stalemate, which occurs when the player whose turn it is to move has no legal move and is not in check.
     * Used for CheckMate, which occurs when player whose turn it is to move is in check and has no legal move to escape check.
     *
     * @param board the board of the current Chess game
     * @param player the player ('w' or 'b') which you want to figure out if they have no more legal moves to make.
     * @return true if the player has no legal moves, false if the player has legal moves.
     */
    public static boolean hasNoLegalMoves(Piece[][] board, char player){

        //For each piece, test every possible move (all 64 squares!) on the board to see if the move is legal.

        for(int i=0; i<8; i++){
            for(int j=0; j<8; j++){
                Piece piece=board[i][j];
                if(piece!=null && piece.color==player){
                    for(int r=0; r<8; r++){
                        for(int c=0; c<8; c++){
                            if(isLegalMove(i, j, r, c, board, player))
                                return false;
                        }
                    }
                }
            }
        }
        return true;
    }

}
