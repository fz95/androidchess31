package com.group31.androidchess;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Dialog for win or draw and saving the game.
 *
 * @author Feiying Zheng (fz95)
 * @author Kevin Cadavillo(kac460)
 */
public class ChessDialogFragment extends DialogFragment {

    public static final String MESSAGE_KEY = "message_key";

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        // Set an EditText view to get user input
        final EditText input = new EditText(getActivity());
        input.setHint("Title");
        final Bundle bundle = getArguments();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage(bundle.getString(MESSAGE_KEY))
                .setPositiveButton("Save Game",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // get user input
                                String title=input.getText().toString();
                                if(title.equals("")){
                                    Toast.makeText(getContext(), "Please enter a title", Toast.LENGTH_LONG).show();
                                    DialogReturn activity = (DialogReturn) getActivity();
                                    activity.saveGame(title,bundle.getString(MESSAGE_KEY));

                                }else{
                                    //to return the result to the Activity, have to cast the activity to the interface:
                                    DialogReturn activity = (DialogReturn) getActivity();
                                    activity.saveGame(title,bundle.getString(MESSAGE_KEY));
                                }


                            }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                //tell Activity to exit game
                                DialogReturn activity = (DialogReturn) getActivity();
                                activity.exitGame();
                                dialog.cancel();
                            }
                        });

        builder.setView(input);

        return builder.create();
    }
}