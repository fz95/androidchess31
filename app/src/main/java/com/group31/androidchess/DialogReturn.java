package com.group31.androidchess;


/**
 * Interface for returning data from dialog.
 *
 * @author Feiying Zheng (fz95)
 * @author Kevin Cadavillo(kac460)
 */
public interface DialogReturn {
    void saveGame(String gameTitle ,String messageKey);
    void exitGame();
    void promoteTo(int row, int col, char piece);
}
