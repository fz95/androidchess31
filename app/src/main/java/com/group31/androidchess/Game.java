package com.group31.androidchess;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;


/**
 * Represents a chess game. Stores all board states of a game.
 *
 * @author Feiying Zheng (fz95)
 * @author Kevin Cadavillo(kac460)
 */
public class Game implements Serializable {
    public ArrayList<Piece[][]> boardStates;
    Date date;
    String title;
    /**
     * if no winner, = 'd'
     * if winner is white, ='w'
     * if winner is black, ='b'
     */
    char winner;
    public Game(){
        this.date=new Date();
        boardStates=new ArrayList<Piece[][]>();
        winner='d';
    }
    public void setTitle(String title){
        this.title=title;
    }
    public void setWinner(char winner){
        this.winner=winner;
    }

    public String toString(){
        return title + ":\t"+date;
    }
}
