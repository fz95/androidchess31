package com.group31.androidchess;
/**
 * Represents the King Piece.
 *
 * @author Feiying Zheng (fz95)
 * @author Kevin Cadavillo(kac460)
 */
public class King extends Piece {
    /**
     * Tracks if the King has moved or not in a game.
     */
    boolean hasMoved=false;

    /**
     * Is set to true when {@link #canMove(int, int, Piece[][])} determines that the move allows castling.
     */
    boolean castling=false;

    /**
     * 3 argument constructor, usually used for making a new King piece when a new Chess game starts.
     *
     * @param currentRank initial rank of the King
     * @param currentFile initial file of the King
     * @param color color of the King (b or w)
     */
    public King(int currentRank, int currentFile, char color) {
        super(currentRank, currentFile, color);
    }

    /**
     * 1 argument constructor, makes a new King piece that is identical to the given piece (same rank and file).
     * Usually used when creating a hypothetical board that is identical to the current Chess board.
     *
     * @param k the King that you want to copy.
     */
    public King(Piece k){
        super(k);
        if(k instanceof King){
            hasMoved=((King) k).hasMoved;
            castling=((King) k).castling;
        }
        else{
            hasMoved=false;
            castling=false;
        }
    }

    /**
     * Determines if King can move to a certain position on the board. Move is not allowed if it puts its own King in check.
     *
     * @param endRank the rank where you want to move the King to.
     * @param endFile the file where you want to move the King to.
     * @param board the board of the current Chess game.
     * @return true if King can move, false if cannot.
     */
    public boolean canMove(int endRank, int endFile, Piece[][] board) {
        if (!(Math.abs(endRank-currentRank)>1 || Math.abs(endFile-currentFile)>1))//usual movement of king
            return true;

        //checks king requirements
        if(endRank==currentRank && Math.abs(endFile-currentFile)==2 && hasMoved==false) { //check castling

            //checks rook's requirements
            if(endFile-currentFile==2){//moving right
                if(!(board[currentRank][7] instanceof Rook && ((Rook)board[currentRank][7]).hasMoved==false))//not a rook or rook has moved
                    return false;
            }
            else if(endFile-currentFile==-2){//moving left
                if(!(board[currentRank][0] instanceof Rook && ((Rook)board[currentRank][0]).hasMoved==false))
                    return false;
            }
            else //
                return false;

            //both king and rook meet requirements for castling so far.
            //must move horizontally straight line without being blocked:
            castling=Rules.straightRuleCheck(currentRank, currentFile, endRank, endFile, board);
            if(castling==false)
                return false;

            //king must not be put in check at any of the positions between start and endFile
            //(note endFile itself is checked in Chess for putting player in check)
            Piece[][] hypBoard=Chess.deepCopyBoard(board);
            if(Chess.inCheck(hypBoard, color)){
                castling=false;
                return false;
            }
            if(endFile<currentFile){
                for(int f=currentFile-1; f>endFile; f--){
                    hypBoard[currentRank][f]=hypBoard[currentRank][f+1];
                    hypBoard[currentRank][f+1]=null;
                    hypBoard[currentRank][f].currentFile=f;
                    if(Chess.inCheck(hypBoard, color)){
                        castling=false;
                        return false;
                    }
                }
            }
            else{
                for(int f=currentFile+1; f<endFile; f++){
                    hypBoard[currentRank][f]=hypBoard[currentRank][f-1];
                    hypBoard[currentRank][f-1]=null;
                    hypBoard[currentRank][f].currentFile=f;
                    if(Chess.inCheck(hypBoard, color)){
                        castling=false;
                        return false;
                    }
                }
            }

            return true;
        }

        return false;
    }
    public int display(){
        if (this.color=='b'){
            return R.drawable.black_king;
        }
        return R.drawable.white_king;
    }

    public String toString() {
        return color+"K";
    }
}