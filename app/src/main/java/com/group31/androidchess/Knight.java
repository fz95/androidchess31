package com.group31.androidchess;
/**
 * Represents the Knight Piece.
 *
 * @author Feiying Zheng (fz95)
 * @author Kevin Cadavillo(kac460)
 */
public class Knight extends Piece {

    /**
     * 3 argument constructor, usually used for making a new Knight piece when a new Chess game starts.
     *
     * @param currentRank initial rank of the Knight
     * @param currentFile initial file of the Knight
     * @param color color of the Knight (b or w)
     */
    public Knight(int currentRank, int currentFile, char color) {
        super(currentRank, currentFile, color);
    }

    /**
     * 1 argument constructor, makes a new Knight piece that is identical to the given piece (same rank and file).
     * Usually used when creating a hypothetical board that is identical to the current Chess board.
     *
     * @param n the Knight that you want to copy.
     */
    public Knight(Piece n){
        super(n);
    }

    public boolean canMove(int endRank, int endFile, Piece[][] board) {

        //2 squares horizontally, 1 square vertically
        if(Math.abs(endRank-currentRank)==2){
            if(Math.abs(endFile-currentFile)==1)
                return true;
        }
        //1 square horizontally, 2 squares vertically
        if(Math.abs(endRank-currentRank)==1){
            if(Math.abs(endFile-currentFile)==2)
                return true;
        }
        return false;
    }
    public int display(){
        if (this.color=='b'){
            return R.drawable.black_knight;
        }
        return R.drawable.white_knight;
    }
    public String toString() {
        return color+"N";
    }
}
