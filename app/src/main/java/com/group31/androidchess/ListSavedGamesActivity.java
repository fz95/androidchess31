package com.group31.androidchess;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.ArrayList;


/**
 * Activity that lists all the saved games, filtered by either title or date.
 *
 * @author Feiying Zheng (fz95)
 * @author Kevin Cadavillo(kac460)
 */
public class ListSavedGamesActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private ListView savedGamesListView;
    private ArrayList<Game> allGames;
    private Spinner sortByDropdown;
    protected static Game selectedGame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            AllGames.loadGames();
        }
        catch(Exception e){
            e.printStackTrace();
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_saved_games);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Saved Games");

        sortByDropdown=findViewById(R.id.sortByDropdown);
        savedGamesListView=findViewById(R.id.savedGamesListView);
        selectedGame=null;

        ArrayAdapter<CharSequence>adapter=ArrayAdapter.createFromResource(this, R.array.sort_by_array, R.layout.game);
        adapter.setDropDownViewResource(R.layout.game);
        sortByDropdown.setAdapter(adapter);
        allGames=AllGames.allGames;

     //   //System.out.println("PRINTING SAVED GAMES");
       // AllGames.printGames();

        sortByDropdown.setOnItemSelectedListener(this);

        savedGamesListView.setAdapter(new ArrayAdapter<Game>(this, R.layout.game, allGames));
        Intent goToSavedGameActivity = new Intent(this, SavedGameActivity.class);
        AllGames.allGames.sort((g1, g2) -> g1.title.compareToIgnoreCase(g2.title));

        savedGamesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> adapter, View v, int position, long id) {

                selectedGame = (Game) savedGamesListView.getItemAtPosition(position);
                savedGamesListView.setSelection(position);
                startActivity(goToSavedGameActivity);
            }
        });
    }

    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id){
        String selection = (String)parent.getItemAtPosition(pos);
        if(selection.equals("Sort By Title")) {
            //System.out.println("Sort by title");
            AllGames.allGames.sort((g1, g2) -> g1.title.compareToIgnoreCase(g2.title));
        }
        else{
            //System.out.println("Sort by DATE");
            AllGames.allGames.sort((g1, g2) -> g1.date.compareTo(g2.date));
        }
        savedGamesListView.setAdapter(new ArrayAdapter<Game>(this, R.layout.game, allGames));
    }
    public void onNothingSelected(AdapterView<?> parent){
        //DO NOTHING
    }
}
