package com.group31.androidchess;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Main menu activity.
 *
 * @author Feiying Zheng (fz95)
 * @author Kevin Cadavillo(kac460)
 */
public class MainActivity extends AppCompatActivity {
    private Button newGameButton;
    private Button savedGamesButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //so it will not be null when we call methods in AllGames
        AllGames.context=this;

        newGameButton = findViewById(R.id.newGameButton);
        savedGamesButton = findViewById(R.id.savedGamesButton);

    }

    public void savedGames( View v){
        Intent intent = new Intent(this, ListSavedGamesActivity.class);
        startActivity(intent);
    }

    public void newGame(View v){
        Intent intent = new Intent(this, NewGameActivity.class);
        startActivity(intent);

    }
}
