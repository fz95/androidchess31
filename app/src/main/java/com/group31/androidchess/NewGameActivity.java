package com.group31.androidchess;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.media.Image;
import android.provider.SyncStateContract;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;
import java.util.stream.IntStream;


/**
 * Activity that handles actual chess game.
 *
 * @author Feiying Zheng (fz95)
 * @author Kevin Cadavillo(kac460)
 */
public class NewGameActivity extends AppCompatActivity implements DialogReturn {

    private Button undoButton;
    private Button AIButton;
    private Button drawButton;
    private Button resignButton;
    private GridLayout gridLayout;
    private TextView turnText;
    private ImageView selectedPos1;
    private ImageView selectedPos2;
    private Game currGame;

    /**
     * true if the most recently executed move was just undone
     * false otherwise
     */
    private boolean moveUndone;

    private char turn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Play");
        AllGames.context=this;

        setContentView(R.layout.activity_new_game);
        selectedPos1=null;
        selectedPos2=null;
        undoButton = findViewById(R.id.undoButton);
        AIButton = findViewById(R.id.AIButton);
        drawButton = findViewById(R.id.drawButton);
        resignButton = findViewById(R.id.resignButton);
        gridLayout = findViewById(R.id.gridLayout);
        turnText=findViewById(R.id.turnText);

        currGame=new Game();
        Chess.start();
        currGame.boardStates.add(Chess.deepCopyBoard(Chess.board));
        printBoard();
        updateBoard();

        moveUndone=false;

        turn='w';
        turnText.setText("White's Turn");

    }

    private void switchTurn(){
        if(turn=='b') {
            turn = 'w';
            turnText.setText("White's Turn");
        }
        else {
            turn = 'b';
            turnText.setText("Black's Turn");
        }
    }

    public void undo(View v){
        //cannot undo the initial board
        if(currGame.boardStates.size()==1){
            Toast.makeText(NewGameActivity.this, "Nothing to undo", Toast.LENGTH_LONG).show();

            return;
        }
        //first board in the list is the initial board; if there's a 2nd board then at least 1 move has been made
        //can only undo 1 move, no further
        ////System.out.println("#boards: " + currGame.boardStates.size());
        if(currGame.boardStates.size()>1 && !moveUndone) {
            //REMOVES the board effected by the undone move from currGame.boardStates:
            currGame.boardStates.remove(currGame.boardStates.size() - 1);
            //Makes previous board state the new board:
           // Chess.board = currGame.boardStates.get(currGame.boardStates.size() - 1);
            Chess.board = Chess.deepCopyBoard(currGame.boardStates.get(currGame.boardStates.size() - 1));
            switchTurn();
            updateBoard();
            moveUndone=true;
        }
        else{
            if(moveUndone) {
                ////System.out.println("CANOT UNDO MORE THAN 1 MOVE AT A TIME");
                Toast.makeText(NewGameActivity.this, "Cannot undo more than 1 move at a time", Toast.LENGTH_LONG).show();
            }
        }
    }
    public void AI(View v) {
        Random r = new Random();
        while (true) {
            int startRow = r.nextInt(8);
            int startCol = r.nextInt(8);
            int endRow = r.nextInt(8);
            int endCol = r.nextInt(8);
            if (Chess.executeMove(startRow, startCol, endRow, endCol, turn)) {
                moveUndone = false;
                updateBoard();
                currGame.boardStates.add(Chess.deepCopyBoard(Chess.board));
                switchTurn();
                checkGameStatus();

                return;
            }
        }
    }

    public void resign(View v){
        String dialogMessage="";
        if(turn=='w') {
           currGame.setWinner('b');
            dialogMessage="BLACK WON";
        }
        else {
            currGame.setWinner('w');
            dialogMessage="WHITE WON";
        }
        showDialog(dialogMessage);

    }
    public void draw(View v){
        showDialog("DRAW");
    }


    private  void printBoard() {
        float heightWidth=TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 336, getResources().getDisplayMetrics());
        gridLayout.removeAllViews();

        gridLayout.setColumnCount(8);
        gridLayout.setRowCount(8);
        boolean isBlack = true;
        for(int i=0; i<8; i++) {
            isBlack=!isBlack;
            for(int j=0; j<8; j++) {
                final ImageView imageView = new ImageView(this);
                imageView.setImageResource(0);
                if (isBlack){
                    ////System.out.print("##");
                    imageView.setBackgroundColor(getColor(R.color.dark));
                }else{
                    ////System.out.print("  ");
                    imageView.setBackgroundColor(getColor(R.color.light));
                }

                GridLayout.LayoutParams param = new GridLayout.LayoutParams();
                param.height =(int)heightWidth/8;
                param.width =(int)heightWidth/8;

                param.setGravity(Gravity.FILL);

                param.columnSpec = GridLayout.spec(j,1f);
                param.rowSpec = GridLayout.spec(i,1f);

                imageView.setLayoutParams(param);

                gridLayout.addView(imageView);

                isBlack=!isBlack;

                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectedBoardPosition(v);
                    }
                });
            }
        }
    }

    private void updateBoard(){
        for(int i=0; i<8; i++) {
            for(int j=0; j<8; j++){
                int gridIndex=getIndexFromPostion(i,j);
                ImageView position= (ImageView) gridLayout.getChildAt(gridIndex);
                position.setImageResource(0);
                position.clearColorFilter();
                position.getBackground().clearColorFilter();

                if(Chess.board[i][j]!=null){
                    position.setImageResource(Chess.board[i][j].display());
                }
            }
        }

    }

    /**
     * Converts row and column into index for the grid layout
     * @param row
     * @param col
     * @return index for the grid layout
     */
    protected static int getIndexFromPostion(int row, int col){
        return (row *8)+ col;
    }

    /**
     * Converts index from the grid layout into the column position
     * @param index from grid layout
     * @return column position from the index
     */
    protected static int getColFromIndex(int index){
        return index % 8;
    }

    /**
     * Converts index from the grid layout into the row position
     * @param index from grid layout
     * @return row position from the index
     */
    private int getRowFromIndex(int index){
        return (int)(index / 8);
    }

    private void selectedBoardPosition(View v){
        ImageView imageView=(ImageView)v;
        int index=gridLayout.indexOfChild(v);
        int row=getRowFromIndex(index);
        int col=getColFromIndex( index);

        //nothing is selected, set 1st position to this one
        if (selectedPos1==null && selectedPos2==null){
            selectedPos1=imageView;
            imageView.setColorFilter(new PorterDuffColorFilter(getColor(R.color.filter), PorterDuff.Mode.MULTIPLY));
            imageView.getBackground().setColorFilter(new PorterDuffColorFilter(getColor(R.color.filter), PorterDuff.Mode.MULTIPLY));

        }
        else if(selectedPos1!=null){
            selectedPos2=imageView;
            imageView.setColorFilter(new PorterDuffColorFilter(getColor(R.color.filter), PorterDuff.Mode.MULTIPLY));
            imageView.getBackground().setColorFilter(new PorterDuffColorFilter(getColor(R.color.filter), PorterDuff.Mode.MULTIPLY));

            //make move
            int startIndex=gridLayout.indexOfChild(selectedPos1);
            int startRow=getRowFromIndex(startIndex);
            int startCol=getColFromIndex(startIndex);

            int endIndex=gridLayout.indexOfChild(selectedPos2);
            int endRow=getRowFromIndex(endIndex);
            int endCol=getColFromIndex(endIndex);
            selectedPos1.clearColorFilter();
            selectedPos1.getBackground().clearColorFilter();
            selectedPos2.clearColorFilter();
            selectedPos2.getBackground().clearColorFilter();
            selectedPos1=null;
            selectedPos2=null;
            if(!Chess.executeMove(startRow, startCol, endRow, endCol, turn)){
                //attempted move was illegal
                ////System.out.println("ILLEGAL MOVE. TRY AGAIN");
                Toast.makeText(NewGameActivity.this, "Illegal move, try again", Toast.LENGTH_LONG).show();

                return;
            }
            else{
                //move was successfully executed
                //check if need to promote
                if(Chess.canPromote(endRow, endCol)){
                    Bundle bundle = new Bundle();
                    bundle.putInt(PromoteDialogFragment.ROW, endRow);
                    bundle.putInt(PromoteDialogFragment.COL, endCol);
                    DialogFragment newFragment = new PromoteDialogFragment();
                    newFragment.setCancelable(false);
                    newFragment.setArguments(bundle);
                    newFragment.show(getSupportFragmentManager(), "promote");

                    //actual promotion is done after the dialog closes
                    return;

                }

                //now that there is a new most recent move, it must be that the most recent move has not been undone
                moveUndone=false;
                updateBoard();
                currGame.boardStates.add(Chess.deepCopyBoard(Chess.board));
                switchTurn();
                checkGameStatus();
                }
               // return;
            }


    }

    private void showDialog(String message){
        Bundle bundle = new Bundle();
        bundle.putString(ChessDialogFragment.MESSAGE_KEY, message);
        DialogFragment newFragment = new ChessDialogFragment();
        newFragment.setCancelable(false);
        newFragment.setArguments(bundle);
        newFragment.show(getSupportFragmentManager(), "savegame");
    }

    //gets called by the ChessDialogFragment when user decides to save the game
    @Override
    public void saveGame(String title, String messageKey) {
        if(title.equals("")){
            showDialog(messageKey);
            return;
        }
        currGame.setTitle(title);
        AllGames.allGames.add(currGame);
        Toast.makeText(NewGameActivity.this, "Saving Game...", Toast.LENGTH_LONG).show();

        try {
            AllGames.saveGames();
        }
        catch(Exception e){
            e.printStackTrace();
        }
        exitGame();

    }
    @Override
    public void exitGame(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void promoteTo(int row, int col, char piece){
        ////System.out.println("PROMOTING TO:"+piece);
        Chess.promote(row, col, piece);
        //now that there is a new most recent move, it must be that the most recent move has not been undone
        moveUndone=false;
        updateBoard();
        currGame.boardStates.add(Chess.deepCopyBoard(Chess.board));
        switchTurn();
        checkGameStatus();
    }

    private void checkGameStatus() {
        int gameStatus = Chess.checkGameStatus(turn);
        //in check
        if(gameStatus==4){
            int[] kingPos=Chess.findKing(Chess.board, turn);
            int kingRow=kingPos[0];
            int kingCol=kingPos[1];

            int kingIndex=getIndexFromPostion(kingRow, kingCol);
            ImageView position=(ImageView)gridLayout.getChildAt(kingIndex);
            position.setColorFilter(new PorterDuffColorFilter(getColor(R.color.redfilter), PorterDuff.Mode.MULTIPLY));
            position.getBackground().setColorFilter(new PorterDuffColorFilter(getColor(R.color.redfilter), PorterDuff.Mode.MULTIPLY));

            if(turn=='b') {
                turnText.setText("Black's Turn: Check");
            }
            else {
                turnText.setText("White's Turn:Check");
            }
            return;
        }
        if (gameStatus != 0) {
            //game is over
            String dialogMessage = "";
            if (gameStatus == 1) {
                ////System.out.println("STALEMATE");
                dialogMessage = "STALEMATE";
            } else if (gameStatus == 2) {
                ////System.out.println("BLACK WON");
                dialogMessage = "BLACK WON";
                currGame.setWinner('b');
            } else if(gameStatus==3){
                ////System.out.println("WHITE WON");
                dialogMessage = "WHITE WON";
                currGame.setWinner('w');
            }
            showDialog(dialogMessage);
        }


    }
}
