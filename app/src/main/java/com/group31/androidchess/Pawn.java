package com.group31.androidchess;
/**
 * Represents the Pawn Piece.
 *
 * @author Feiying Zheng (fz95)
 * @author Kevin Cadavillo(kac460)
 */
public class Pawn extends Piece {

    /**
     * Stores the Pawn's starting rank.
     */
    private int startRank;

    /**
     * Is set to true when {@link #canMove(int, int, Piece[][])} determines that the move is en passant.
     */
    boolean enPassant=false;

    /**
     * 3 argument constructor, usually used for making a new Pawn piece when a new Chess game starts.
     *
     * @param currentRank initial rank of the Pawn
     * @param currentFile initial file of the Pawn
     * @param color color of the Pawn (b or w)
     */
    public Pawn(int currentRank, int currentFile, char color) {
        super(currentRank, currentFile, color);
        startRank=currentRank;
    }

    /**
     * 1 argument constructor, makes a new Pawn piece that is identical to the given piece (same rank and file).
     * Usually used when creating a hypothetical board that is identical to the current Chess board.
     *
     * @param p the Pawn that you want to copy.
     */
    public Pawn(Piece p){
        super(p);
        if(p instanceof Pawn)
            startRank=((Pawn) p).startRank;
        else//should never happen!
            startRank=currentRank;
    }

    public boolean canMove(int endRank, int endFile, Piece[][] board) {
        int forward=1; //black moves down board (positive change in array index)
        if(color=='w')
            forward=-1; //white moves up board (negative change in array index)

        //Capturing diag and en passant:
        if(Math.abs(endFile-currentFile)==1){ //can ONLY capture in adjacent file
            if(endRank==currentRank+forward){ //can ONLY capture 1 rank forward
                if(board[endRank][endFile]!=null) //can ONLY capture if there is a to-be-captured piece in diag
                    return true; //don't need to make sure that to-be-captured piece is enemy piece--done in Chess.

                //en passant capture. The square diag to pawn is empty. Need to check if the square next to pawn has a piece to capture
                if(board[endRank][endFile]==null && board[currentRank][endFile]!=null) {
                    //Checks to make sure en passant can only be made immediately after the opponent made a double advance on their pawn.
                    if(Chess.doubleAdvancePawn==board[currentRank][endFile]) {
                        enPassant=true;
                        return true;
                    }
                }
            }
            return false; //conditions above not met => trying to change file without capturing => illegal move
        }

        if(endFile!=currentFile) //can ONLY change file if capturing (which we've determined isn't the case since above did not return)
            return false;

        //Can move 2 forward from initial rank:
        if(currentRank == startRank && endRank == currentRank+(forward*2)){
            if(board[currentRank+forward][endFile] != null) //blocked
                return false;
            if(board[endRank][endFile] != null) //blocked (CANNOT CAPTURE MOVING FORWARD)
                return false;
            return true;
        }

        //Standard move of pawn is moving 1 forward
        if(endRank == currentRank+forward){
            if(board[endRank][endFile] != null) //blocked (CANNOT CAPTURE MOVING FORWARD)
                return false;
            return true;
        }
        return false;
    }
    public int display(){
        if (this.color=='b'){
            return R.drawable.black_pawn;
        }
        return R.drawable.white_pawn;
    }
    public String toString() {
        return color+"p";
    }
}
