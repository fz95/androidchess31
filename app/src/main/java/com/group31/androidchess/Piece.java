package com.group31.androidchess;

import java.io.Serializable;

/**
 * Abstract class for Chess pieces.
 *
 * @author Feiying Zheng (fz95)
 * @author Kevin Cadavillo(kac460)
 */
public abstract class Piece implements Serializable {
    /**
     * This piece's current rank
     */
    int currentRank;
    /**
     *  This piece's current file
     */
    int currentFile;
    /**
     *  This piece's color (b or w)
     */
    char color;

    /**
     * 3 argument constructor, usually used for making a new piece when a new Chess game starts.
     *
     * @param currentRank initial rank of the piece
     * @param currentFile initial file of the piece
     * @param color color of the piece (b or w)
     */
    public Piece(int currentRank, int currentFile, char color) {
        this.currentRank=currentRank;
        this.currentFile=currentFile;
        this.color=color;
    }

    /**
     * 1 argument constructor, makes a new piece that is identical to the given piece (same rank and file).
     * Usually used when creating a hypothetical board that is identical to the current Chess board.
     *
     * @param piece the piece that you want to copy.
     */
    public Piece(Piece piece){
        this.currentRank=piece.currentRank;
        this.currentFile=piece.currentFile;
        this.color=piece.color;
    }

    /**
     * Determines if a piece can move to a certain position on the board.
     *
     * @param endRank the rank where you want to move the piece to.
     * @param endFile the file where you want to move the piece to.
     * @param board the board of the current Chess game.
     * @return true if piece can move, false if cannot.
     */
    public abstract boolean canMove(int endRank, int endFile, Piece[][] board);

    public abstract int display();
}
