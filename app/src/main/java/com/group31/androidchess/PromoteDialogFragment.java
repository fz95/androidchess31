package com.group31.androidchess;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


/**
 * Dialog for promoting a pawn.
 *
 * @author Feiying Zheng (fz95)
 * @author Kevin Cadavillo(kac460)
 */
public class PromoteDialogFragment extends DialogFragment {
    public static final String ROW="row";
    public static final String COL="col";
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Bundle bundle = getArguments();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.promote_dialog, null);

        final Spinner promoteDropdown=view.findViewById(R.id.promoteDropdown);

        List<String> promoteList = new ArrayList<String>();
        promoteList.add("Queen");
        promoteList.add("Rook");
        promoteList.add("Knight");
        promoteList.add("Bishop");

        ArrayAdapter<String> promoteAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.game, promoteList);

        promoteAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        promoteDropdown.setAdapter(promoteAdapter);

        builder.setMessage("Pawn Promotion")
                .setPositiveButton("Promote",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                int row =bundle.getInt(ROW);
                                int col =bundle.getInt(COL);

                                String selected=promoteDropdown.getSelectedItem().toString();
                                char selectedPiece;
                                if(selected.equals("Rook")){
                                    selectedPiece='R';
                                }else if(selected.equals("Knight")){
                                    selectedPiece='N';
                                }else if(selected.equals("Bishop")){
                                    selectedPiece='B';
                                }else{
                                    selectedPiece='Q';
                                }
                                DialogReturn activity = (DialogReturn) getActivity();
                                activity.promoteTo(row, col,selectedPiece);
                            }
                        });

        builder.setView(view);

        return builder.create();

    }

}
