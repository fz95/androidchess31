package com.group31.androidchess;
/**
 * Represents the Queen Piece.
 *
 * @author Feiying Zheng (fz95)
 * @author Kevin Cadavillo(kac460)
 */
public class Queen extends Piece {

    /**
     * 3 argument constructor, usually used for making a new Queen piece when a new Chess game starts.
     *
     * @param currentRank initial rank of the Queen
     * @param currentFile initial file of the Queen
     * @param color color of the Queen (b or w)
     */
    public Queen(int currentRank, int currentFile, char color) {
        super(currentRank, currentFile, color);
    }

    /**
     * 1 argument constructor, makes a new Queen piece that is identical to the given piece (same rank and file).
     * Usually used when creating a hypothetical board that is identical to the current Chess board.
     *
     * @param q the Queen that you want to copy.
     */
    public Queen(Piece q){
        super(q);
    }
    public boolean canMove(int endRank, int endFile, Piece[][] board) {
        return Rules.straightRuleCheck(currentRank, currentFile, endRank, endFile, board) ||
                Rules.diagRuleCheck(currentRank, currentFile, endRank, endFile, board);
    }
    public int display(){
        if (this.color=='b'){
            return R.drawable.black_queen;
        }
        return R.drawable.white_queen;
    }
    public String toString() {
        return color+"Q";
    }

}
