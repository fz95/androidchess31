package com.group31.androidchess;
/**
 * Represents the Rook Piece.
 *
 * @author Feiying Zheng (fz95)
 * @author Kevin Cadavillo(kac460)
 */
public class Rook extends Piece {
    /**
     * Tracks if the Rook has moved or not in a game.
     */
    boolean hasMoved=false;

    /**
     * 3 argument constructor, usually used for making a new Rook piece when a new Chess game starts.
     *
     * @param currentRank initial rank of the Rook
     * @param currentFile initial file of the Rook
     * @param color color of the Rook (b or w)
     */
    public Rook(int currentRank, int currentFile, char color) {
        super(currentRank, currentFile, color);
    }

    /**
     * 1 argument constructor, makes a new Rook piece that is identical to the given piece (same rank and file).
     * Usually used when creating a hypothetical board that is identical to the current Chess board.
     *
     * @param r the Rook that you want to copy.
     */
    public Rook(Piece r){
        super(r);
        if(r instanceof Rook)
            hasMoved=((Rook) r).hasMoved;
        else
            hasMoved=false;
    }

    public boolean canMove(int endRank, int endFile, Piece[][] board) {
        return Rules.straightRuleCheck(currentRank, currentFile, endRank, endFile, board);
    }
    public int display(){
        if (this.color=='b'){
            return R.drawable.black_rook;
        }
        return R.drawable.white_rook;
    }
    public String toString() {
        return color+"R";
    }

}