package com.group31.androidchess;
/**
 * Contains diagonal and straight (horizontal and vertical) movement rules for a Chess game.
 *
 * @author Feiying Zheng (fz95)
 * @author Kevin Cadavillo(kac460)
 */
public class Rules {

    /**
     * Checks to see if a given move follows the straight(horizontal or vertical) rule and is not blocked by other Chess pieces.
     *
     * @param startRank starting rank of the piece that the player wants to move.
     * @param startFile starting file of the piece that the player wants to move.
     * @param endRank rank where the player wants to move the piece to.
     * @param endFile file where the player wants to move the piece to.
     * @param board the board of the current Chess game
     * @return true if the move satisfies straight rule and is not blocked, false otherwise
     */
    public static boolean straightRuleCheck(int startRank, int startFile, int endRank, int endFile, Piece[][] board) {
        if(startRank!=endRank && startFile!=endFile)
            return false;
        if(startRank==endRank) { //moving left/right
            if(endFile>startFile) {//moving right
                for(int i=startFile+1; i<endFile; i++) {
                    if(board[startRank][i]!=null)
                        return false;//blocked
                }
            }
            else { //moving left
                for(int i=startFile-1; i>endFile; i--) {
                    if(board[startRank][i]!=null)
                        return false;//blocked
                }
            }
        }
        else {
            if(endRank>startRank) {//moving up
                for(int i=startRank+1; i<endRank; i++) {
                    if(board[i][startFile]!=null)
                        return false;//blocked
                }
            }
            else{//moving down
                for(int i=startRank-1; i>endRank; i--) {
                    if(board[i][startFile]!=null)
                        return false;//blocked
                }
            }
        }
        return true;

    }

    /**
     * Checks to see if a given move follows the diagonal rule and is not blocked by other Chess pieces.
     *
     * @param startRank starting rank of the piece that the player wants to move.
     * @param startFile starting file of the piece that the player wants to move.
     * @param endRank rank where the player wants to move the piece to.
     * @param endFile file where the player wants to move the piece to.
     * @param board the board of the current Chess game
     * @return true if the move satisfies diagonal rule and is not blocked, false otherwise
     */
    public static boolean diagRuleCheck(int startRank, int startFile, int endRank, int endFile, Piece[][] board) {
        if (Math.abs(startRank-endRank)!=Math.abs(startFile-endFile)) {
            return false;
        }
        if(endRank<startRank) { //moving up
            if(endFile<startFile) { //moving left
                for (int i=1; startFile-i>endFile;i++) {//checking down and to right from final pos
                    if(board[startRank-i][startFile-i]!=null)
                        return false;
                }

            }else {//moving right
                for(int i=1; startFile+i<endFile; i++) {
                    if(board[startRank-i][startFile+i]!=null)
                        return false;
                }
            }

        }
        else { //moving down
            if(endFile<startFile) { //moving left
                for (int i=1; startFile-i>endFile;i++) {//checking down and to right from final pos
                    if(board[startRank+i][startFile-i]!=null)
                        return false;
                }

            }else {//moving right
                for(int i=1; startFile+i<endFile; i++) {
                    if(board[startRank+i][startFile+i]!=null)
                        return false;
                }
            }
        }
        return true;

    }
}
