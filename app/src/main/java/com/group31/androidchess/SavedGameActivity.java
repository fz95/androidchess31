package com.group31.androidchess;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


/**
 * Playback activity.
 *
 * @author Feiying Zheng (fz95)
 * @author Kevin Cadavillo(kac460)
 */
public class SavedGameActivity extends AppCompatActivity {

    private Button prevButton;
    private Button nextButton;
    private GridLayout gridLayout;
    private TextView winnerTextView;
    private static Game game;
    private static Piece[][] currBoard;
    private int counter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_game);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        prevButton=findViewById(R.id.prevButton);
        nextButton=findViewById(R.id.nextButton);
        gridLayout = findViewById(R.id.gridLayout);
        winnerTextView=findViewById(R.id.winnerTextView);

        game = ListSavedGamesActivity.selectedGame;
        getSupportActionBar().setTitle("Replay Game: "+game.title);

        counter=0;
        currBoard=game.boardStates.get(counter);

        //System.out.println("VIEWING " + game.title);

        printBoard();
        updateBoard();
    }

    public void next(View v){
        if(counter+1<game.boardStates.size()) {
            counter++;
            currBoard = game.boardStates.get(counter);
            updateBoard();
        }
        else{
            //System.out.println("NO NEXT MOVE");
            Toast.makeText(SavedGameActivity.this, "No next move", Toast.LENGTH_LONG).show();

        }
        if(counter+1==game.boardStates.size()){
            if(game.winner=='d'){
                //System.out.println("DRAW");
                winnerTextView.setText("Draw");
            }
            else if(game.winner=='w'){
                //System.out.println("WHITE WON");
                winnerTextView.setText("White Won");
            }
            else{
                //System.out.println("BLACK WON");
                winnerTextView.setText("Black Won");
            }
        }

    }
    public void previous(View v){
        if(counter >= 1) {
            counter--;
            currBoard = game.boardStates.get(counter);
            updateBoard();
        }
        else{
            //System.out.println("NO PREVIOUS MOVE");
            Toast.makeText(SavedGameActivity.this, "No previous move", Toast.LENGTH_LONG).show();

        }
    }

    private void updateBoard(){
        winnerTextView.setText("");
        for(int i=0; i<8; i++) {
            for(int j=0; j<8; j++){
                int gridIndex=NewGameActivity.getIndexFromPostion(i,j);
                ImageView position= (ImageView) gridLayout.getChildAt(gridIndex);
                position.setImageResource(0);
                position.clearColorFilter();
                position.getBackground().clearColorFilter();

                if(currBoard[i][j]!=null){
                    position.setImageResource(currBoard[i][j].display());
                }
            }
        }

    }
    private  void printBoard() {
        float heightWidth= TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 336, getResources().getDisplayMetrics());
        gridLayout.removeAllViews();

        gridLayout.setColumnCount(8);
        gridLayout.setRowCount(8);
        boolean isBlack = true;
        for(int i=0; i<8; i++) {
            isBlack=!isBlack;
            for(int j=0; j<8; j++) {
                final ImageView imageView = new ImageView(this);
                imageView.setImageResource(0);
                if (isBlack){
                    //System.out.print("##");
                    imageView.setBackgroundColor(getColor(R.color.dark));
                }else{
                    //System.out.print("  ");
                    imageView.setBackgroundColor(getColor(R.color.light));
                }

                GridLayout.LayoutParams param = new GridLayout.LayoutParams();
                param.height =(int)heightWidth/8;
                param.width =(int)heightWidth/8;

                param.setGravity(Gravity.FILL);

                param.columnSpec = GridLayout.spec(j,1f);
                param.rowSpec = GridLayout.spec(i,1f);

                imageView.setLayoutParams(param);

                gridLayout.addView(imageView);

                isBlack=!isBlack;

            }
        }
    }
}
